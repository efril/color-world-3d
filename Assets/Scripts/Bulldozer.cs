﻿using UnityEngine;

public class Bulldozer : MonoBehaviour
{
    private void OnTriggerEnter(Collider collision)
    {
        if (collision.gameObject.layer == 8 || collision.gameObject.layer == 9 || collision.gameObject.layer == 12)
        {
            //Destroy(collision.gameObject);
            collision.gameObject.SetActive(false);
        }
    }
}
