﻿using UnityEngine;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{
    [SerializeField] GameObject PaintBar;
    [SerializeField] Image LevelProgressbar;
    [SerializeField] GameObject FinishLine;
    [SerializeField] Text ScoreField;
    [SerializeField] TextMesh LevelStartField;

    [SerializeField] Text GameoverScoreField;
    [SerializeField] Text GameoverHighScoreField;
    [SerializeField] Text FinishScoreField;
    [SerializeField] Text FinishHighScoreField;
    [SerializeField] Text CurrentLevelField;
    [SerializeField] Text NextLevelField;

    Image paintGradientImage;
    Sprite sprite;
    //Material playerMaterial;
    TrailRenderer playerTrail;
    //Texture2D gradientTexture;
    public static UIManager Instance { get; private set; }

    private void Awake()
    {
        if (Instance == null) { Instance = this; }
    }
    void Start()
    {
        if (Instance != this) Destroy(gameObject);

        //sprite = Sprite.Create(CreateGradientTexture("ColorGradient", 50, 300, Color.white, Settings.Instance.PlayerColor), new Rect(0, 0, 50, 300), new Vector2(0.5f, 0.5f));
        //PaintBar.GetComponent<Image>().sprite = sprite;
        //paintGradientImage = PaintBar.GetComponent<Image>();
        //playerMaterial = GameManager.Instance.PlayerSkin.GetComponent<Renderer>().material;
        playerTrail = GameManager.Instance.PlayerSkin.GetComponent<TrailRenderer>();
        ScoreField.text = ScoreSystem.Instance.GetPlayerScore().ToString();
        LevelStartField.text = "Stage " + (Player.Instance.CurrentLevel + 1).ToString();
        CurrentLevelField.text = (Player.Instance.CurrentLevel + 1).ToString();
        NextLevelField.text = (Player.Instance.CurrentLevel + 2).ToString();
    }

    public Texture2D CreateGradientTexture(string name, int width, int height, Color leftColor, Color rightColor)
    {
        Texture2D texture2D = new Texture2D(width, height, TextureFormat.ARGB32, false);
        texture2D.name = name;
        texture2D.hideFlags = HideFlags.HideAndDontSave;
        Color[] array = new Color[width * height];

        for(int i = 0; i < width; i++)
        {
            Color color = Color.Lerp(leftColor, rightColor, (float)i / (float)(width - 1));

            for (int j = 0; j < height; j++)
            {
                array[j * width + i] = color;
            }

        }

        texture2D.SetPixels(array);
        texture2D.wrapMode = TextureWrapMode.Clamp;
        texture2D.Apply();

        //gradientTexture = texture2D;

        return texture2D;
    }

    public void UpdateGameoverUI()
    {
        GameoverScoreField.text = Player.Instance.Score.ToString();
        GameoverHighScoreField.text = Player.Instance.HighScore.ToString();
    }

    public void UpdateLevelcompleteUI()
    {
        FinishScoreField.text = Player.Instance.Score.ToString();
        FinishHighScoreField.text = Player.Instance.HighScore.ToString();
    }

    private void LateUpdate()
    {
        //paintGradientImage.fillAmount = GameManager.Instance.PlayerLives / Settings.Instance.MaxPaint;
        //playerMaterial.color = gradientTexture.GetPixelBilinear(paintGradientImage.fillAmount, 0.5f);
        //playerTrail.startColor = playerMaterial.color;
        //LevelProgressbar.fillAmount = (GameManager.Instance.PlayerSkin.transform.position.z + 1) / FinishLine.transform.position.z;

        //if (paintGradientImage.fillAmount > Settings.Instance.BallMinSize && Settings.Instance.LowerSizeOnHit)
        //{
        //    GameManager.Instance.PlayerSkin.transform.localScale = Vector3.one * paintGradientImage.fillAmount;
        //    playerTrail.widthMultiplier = paintGradientImage.fillAmount;
        //}

        ScoreField.text = ScoreSystem.Instance.GetPlayerScore().ToString();
    }
}
