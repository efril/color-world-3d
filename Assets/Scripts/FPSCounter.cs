﻿using UnityEngine;
using UnityEngine.UI;

public class FPSCounter : MonoBehaviour
{
    [SerializeField] Text TextField;
    [SerializeField] Text BenchmarkTextField;
    float deltaTime = 0.0f;
    float FPS = 0.0f;
    float minFPS = 60f;
    string FPStext = "";
    string benchmarkText = "Waiting for cache...";
    bool waitingForCache = true;

    private void Start()
    {
        InvokeRepeating("UpdateFPS", 3f, 1f);
        UpdateFPS();
    }

    void Update()
    {
        if (waitingForCache) return;

        deltaTime += (Time.deltaTime - deltaTime) * 0.1f;
        FPS = 1.0f / deltaTime;

        if (FPS < 10) TextField.color = Color.red;
        if (FPS > 10 && FPS < 25) TextField.color = Color.yellow;
        if (FPS > 25) TextField.color = Color.white;

        if (FPS < minFPS) minFPS = FPS;

        FPStext = "FPS: " + Mathf.RoundToInt(FPS).ToString();
        benchmarkText = "Min: " + Mathf.RoundToInt(minFPS).ToString() + " Avg:" + Mathf.RoundToInt((60 + minFPS) / 2).ToString();
    }
        
    void UpdateFPS()
    {
        TextField.text = FPStext;
        BenchmarkTextField.text = benchmarkText;
        waitingForCache = false;
    }
}