﻿using UnityEngine;

public class PaintBucket : MonoBehaviour
{
    bool isCollided = false;
    private void OnTriggerEnter(Collider collision)
    {
        if (collision.gameObject.layer == 11)
        {
            if (isCollided) return;
            isCollided = true;

            GameManager.Instance.AddPaint();
            //Destroy(gameObject);
            gameObject.SetActive(false);
        }
    }

}
