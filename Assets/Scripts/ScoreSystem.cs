﻿using UnityEngine;

public class ScoreSystem : MonoBehaviour
{
    int playerScore = 0;
    int godmodeScore = 0;
    float localTimer = -1f;

    public static ScoreSystem Instance { get; private set; }

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
            //DontDestroyOnLoad(gameObject);
        }
    }

    private void Start()
    {
        if (Instance != this) Destroy(gameObject);
        playerScore = Player.Instance.Score;
    }

    public int GetPlayerScore()
    {
        return playerScore;
    }

    public void ErasePlayerScore()
    {
        playerScore = 0;
        Player.Instance.Score = playerScore;
    }    

    public void AddScore()
    {
        if (!GameManager.Instance.IsPlaying) return;

        playerScore += Settings.Instance.ScoreGain;
        godmodeScore += Settings.Instance.ScoreGain;

        Player.Instance.Score = playerScore;
        if (Player.Instance.HighScore < playerScore) Player.Instance.HighScore = playerScore;

        if (localTimer > 0f)
        {
            playerScore += Settings.Instance.BonusScore;
            godmodeScore += Settings.Instance.BonusScore;
            localTimer = 0f;
            Player.Instance.Score = playerScore;
            return;
        }

        localTimer = 0f;

    }

    private void Update()
    {
        if (localTimer >= 0f) localTimer += Time.deltaTime;
        if (localTimer >= Settings.Instance.BonusTimer) localTimer = -1f;

        if (GameManager.Instance.IsGodmodeActive()) godmodeScore = 0;

        if (godmodeScore >= Settings.Instance.ScoreToGain)
        {
            godmodeScore = godmodeScore - Settings.Instance.ScoreToGain;
            GameManager.Instance.ActivateGodmode();
        }

    }

}
