﻿using UnityEngine;

public class BallMover : MonoBehaviour
{
    Vector3 offset;
    Vector3 speed;
    Rigidbody _rb;

    private void Start() {
        _rb = gameObject.GetComponent<Rigidbody>();
    }

    void Update()
    {
        if (Input.GetMouseButtonDown(0)) {
            var ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;
            if (Physics.Raycast(ray, out hit)) {
                offset = hit.point - transform.position;
            }
        }

        var newSpeed = Vector3.zero;
        if (Input.GetMouseButton(0) && GameManager.Instance.IsPlaying) {
            var ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;
            if (Physics.Raycast(ray, out hit)) {
                newSpeed = hit.point - offset - transform.position;
                newSpeed.y = 0;
            }
            speed = Vector3.Lerp(speed, newSpeed, Settings.Instance.DirectionChangeSpeed);
        } else {
            speed = Vector3.Lerp(speed, newSpeed, Time.deltaTime);
        }
    }

    private void FixedUpdate() {
        if (GameManager.Instance.IsPlaying)
        {
            var s = speed * Settings.Instance.MoveSpeed + CameraMover.Instance.MoveSpeed;
            s.y = _rb.velocity.y;
            _rb.velocity = s;
        }
        else { speed = Vector3.zero; }
    }

}
