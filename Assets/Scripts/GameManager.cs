﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class GameManager : MonoBehaviour
{
    public static GameManager Instance { get; private set; }

    [SerializeField] GameObject LevelCompleteUI = null;
    [SerializeField] GameObject LevelFailedUI = null;

    public float PlayerLives;
    //[SerializeField] Text PlayerLivesText;

    public GameObject PlayerSkin;
    public GameObject GodModeFX;

    float godmodeTimer = -1f;
    bool godmodeActive = false;

    [SerializeField] GameObject RevivePosition;
    [SerializeField] GameObject DeathPosition;
    [SerializeField] GameObject BestZPosition;

    public GameObject ElementDeathFX;

    [SerializeField] List<GameObject> _levels;

    enum GameMode{
        awaitStart, play, endLose, endWin
    }

    GameMode _mode = GameMode.awaitStart;

    private void Awake()
    {
        if (Instance == null) Instance = this;
    }

    private void Start()
    {
        if (Instance != this) Destroy(gameObject);
        PlayerLives = Settings.Instance.MaxPaint;
        BestZPosition.transform.LeanMoveZ(Player.Instance.BestZ, 0f);

        if (!Settings.Instance.TestLevelMode) {
            var playerLevel = Math.Min(Player.Instance.CurrentLevel, _levels.Count - 1);
            for (var i = 0; i < _levels.Count; i++) {
                if (i == playerLevel) {
                    _levels[i].SetActive(true);
                    CameraMover.Instance.Level = _levels[i];
                } else {
                    _levels[i].SetActive(false);
                }
            }
        }
    }

    public void UpdateBestZ()
    {
        BestZPosition.transform.LeanMoveZ(Player.Instance.BestZ, 1f).setEaseInSine();
    }


    public void StartLevel()
    {
        _mode = GameMode.play;
    }

    public void LevelCompleted()
    {
        Player.Instance.Score = ScoreSystem.Instance.GetPlayerScore();
        LevelCompleteUI.SetActive(true);
        if(Player.Instance.CurrentLevel < _levels.Count - 1) Player.Instance.CurrentLevel++;
        UIManager.Instance.UpdateLevelcompleteUI();
        _mode = GameMode.endWin;
        Player.Instance.BestZ = -30f;
        UpdateBestZ();
        Debug.Log("Level completed");
    }

    public void RevivePlayer()
    {
        
        PlayerSkin.GetComponent<TrailRenderer>().enabled = true;
        PlayerSkin.GetComponent<BallMover>().enabled = true;
        PlayerSkin.GetComponent<SphereCollider>().enabled = true;
        PlayerSkin.GetComponent<BoxCollider>().enabled = true;
        PlayerSkin.GetComponent<Rigidbody>().useGravity = true;
        PlayerSkin.GetComponent<Rigidbody>().WakeUp();
        LeanTween.value(Time.timeScale, 1f, 1f);
        LeanTween.move(PlayerSkin, RevivePosition.transform.position, 1f).setEaseInExpo().setOnComplete(() => {
            _mode = GameMode.play;
            Explode(RevivePosition.transform.position);
        });
        PlayerLives = Settings.Instance.MaxPaint;
        ActivateGodmode();
        
    }

    public void PlayerDied()
    {
        _mode = GameMode.endLose;
        
        LeanTween.value(Time.timeScale, 0.5f, 1f);

        Explode(PlayerSkin.transform.position);

        if (Player.Instance.BestZ < PlayerSkin.transform.position.z) Player.Instance.BestZ = PlayerSkin.transform.position.z;
        UpdateBestZ();

        PlayerSkin.GetComponent<TrailRenderer>().enabled = false;
        PlayerSkin.GetComponent<BallMover>().enabled = false;
        PlayerSkin.GetComponent<SphereCollider>().enabled = false;
        PlayerSkin.GetComponent<BoxCollider>().enabled = false;
        PlayerSkin.GetComponent<Rigidbody>().useGravity = false;
        PlayerSkin.transform.position = DeathPosition.transform.position;
        PlayerSkin.GetComponent<Rigidbody>().velocity = Vector3.zero;
        PlayerSkin.GetComponent<Rigidbody>().Sleep();


        StartCoroutine(LevelFailed());
    }

    void Explode(Vector3 position)
    {
        Collider[] colliders = Physics.OverlapSphere(position, 7.5f);

        foreach (Collider hit in colliders)
        {
            Rigidbody rb = hit.GetComponent<Rigidbody>();
            if (rb != null) rb.AddExplosionForce(10.0f, position, 7.5f, 7.5f, ForceMode.Impulse);
        }
    }
        
    IEnumerator LevelFailed()
    {
        yield return new WaitForSecondsRealtime(1.5f);
        LevelFailedUI.SetActive(true);
        UIManager.Instance.UpdateGameoverUI();
    }

    void CheckLives()
    {
        if (PlayerLives <= 0f)
        {
            PlayerLives = 0f;
            PlayerDied();
        }
    }

    public void LooseLive()
    {
        if (Settings.Instance.InfiniteGodMode) return;
        if (IsGodmodeActive()) return;

        PlayerLives -= Settings.Instance.PaintLooseSpeed;// * Time.deltaTime;
        CheckLives();
    }

    public void SpendPaint()
    {
        if (Settings.Instance.InfiniteGodMode) return;
        if (IsGodmodeActive()) return;

        PlayerLives -= Settings.Instance.PaintSpendSpeed * Time.deltaTime;
        CheckLives();
        
    }

    public void ActivateGodmode()
    {
        if (godmodeTimer == -1)
        {
            godmodeTimer = Settings.Instance.TimeActivated;
            godmodeActive = true;
            GodModeFX.SetActive(true);
        }
                
    }
        
    private void Update()
    {
        if (godmodeTimer > -1) godmodeTimer -= Time.deltaTime;

        if (godmodeTimer <= 0f)
        {
            godmodeTimer = -1;
            godmodeActive = false;
            GodModeFX.SetActive(false);
        }
                    
    }

    public float BoosterSpeedUp()
    {
        if (IsGodmodeActive()) return (1 + Settings.Instance.SpeedUpBy / 100f);
        return 1;
    }

    public void AddPaint()
    {
        PlayerLives += Settings.Instance.PaintBucketHeals;
        if (PlayerLives > Settings.Instance.MaxPaint) PlayerLives = Settings.Instance.MaxPaint;
    }   

    public bool IsPlaying { get => _mode == GameMode.play; }
    public bool IsGodmodeActive() { return godmodeActive; }

   

}
