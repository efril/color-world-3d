﻿using UnityEngine;

public class PaintedKiller : MonoBehaviour
{

    GameObject deathFXobject;
    ParticleSystem deathFX;
    ParticleSystemRenderer FXRender;
    
    void Start()
    {
        Destroy(GetComponent<Rigidbody>());

        deathFXobject = Instantiate(GameManager.Instance.ElementDeathFX, transform);
        deathFXobject.transform.position = gameObject.GetComponent<Renderer>().bounds.center;
               
        deathFX = deathFXobject.GetComponent<ParticleSystem>();
        FXRender = deathFXobject.GetComponent<ParticleSystemRenderer>();
        FXRender.material = gameObject.GetComponent<Renderer>().material;

        gameObject.layer = 0;

        gameObject.GetComponent<Renderer>().enabled = false;

        deathFX.Play();
                        
        //Destroy(gameObject, Settings.Instance.BadKillsIn);

    }

    private void OnParticleSystemStopped() {
        gameObject.SetActive(false);
    }
}
