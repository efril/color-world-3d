﻿using UnityEngine;

public class SwipeTip : MonoBehaviour
{
    void Start()
    {
        LeanTween.moveLocalX(gameObject, 2f, 1f).setLoopPingPong().setEaseInSine();
    }

    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            GameManager.Instance.StartLevel();
            Destroy(gameObject);
        }
    }
}
