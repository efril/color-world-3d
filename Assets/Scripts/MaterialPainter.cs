﻿using UnityEngine;

public class MaterialPainter : MonoBehaviour
{
            
    private void OnCollisionEnter(Collision collision)
    {
        //if (collision.gameObject.layer == 8)
        //{
        //    gameObject.GetComponent<Renderer>().material = Settings.Instance.BadMaterial;
        //    return;
        //}

        if (collision.gameObject.layer == 9)
        {
            if (collision.gameObject.GetComponent<MaterialPainter>() == null) collision.gameObject.AddComponent<MaterialPainter>();
            collision.gameObject.GetComponent<Renderer>().material = GetComponent<Renderer>().material;
            collision.gameObject.layer = 12;

            ScoreSystem.Instance.AddScore();

            return;
        }
        
    }
}
