﻿using UnityEngine;

public class BadPainter : MonoBehaviour
{
    private void OnCollisionEnter(Collision collision)
    {
        if (!GameManager.Instance.IsPlaying) return;

        //if (collision.gameObject.layer == 12)
        //{
        //    if (collision.gameObject.GetComponent<PaintedKiller>() == null) collision.gameObject.AddComponent<PaintedKiller>();
        //    return;
        //}      

        if (collision.gameObject.layer == 9)
        {
            if (collision.gameObject.GetComponent<BadPainter>() == null) collision.gameObject.AddComponent<BadPainter>();
            collision.gameObject.GetComponent<Renderer>().material = GetComponent<Renderer>().material;
            collision.gameObject.layer = 8;
            return;
        }

    }
}
