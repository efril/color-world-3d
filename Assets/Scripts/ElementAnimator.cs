﻿using UnityEngine;

public class ElementAnimator : MonoBehaviour
{
    public enum AnimationTypes
    {
        Rotation,
        MoveLooped
    };   

    public AnimationTypes Type;
    [Space]
    [Header("Rotation")]
    public float RotationSpeed = 50f;
    public Vector3 RotationAxis = new Vector3(0f, 1f, 0f);
    [Space]
    [Header("Move Looped")]
    public Vector3 MoveDirection;
    public float ForwardSpeed;
    public float BackwardsSpeed;
    public float PushOnContact = 0f;
    int forwardID;
    int backwardsID;

    int tweenID = -1;
    Vector3 initPos;

    Rigidbody rb;
        
    private void Start()
    {
        
        rb = GetComponent<Rigidbody>();
        rb.useGravity = false;
        rb.Sleep();

        initPos = transform.position;
                
            switch (Type)
            {
                case AnimationTypes.MoveLooped:

                    forwardID = LeanTween.move(gameObject, initPos + MoveDirection, ForwardSpeed).setOnComplete(MoveBackwards).id;
                        break;
            }
        
    }

    void MoveForward()
    {
        forwardID = LeanTween.move(gameObject, initPos + MoveDirection, ForwardSpeed).setOnComplete(MoveBackwards).id;
    }

    void MoveBackwards()
    {
        backwardsID = LeanTween.move(gameObject, initPos, BackwardsSpeed).setOnComplete(MoveForward).id;
    }

    void Update()
    {
        switch (Type)
        {
            case AnimationTypes.MoveLooped:

                if (GameManager.Instance.IsPlaying)
                {
                    LeanTween.resume(forwardID);
                    LeanTween.resume(backwardsID);
                }
                else
                {
                    LeanTween.pause(forwardID);
                    LeanTween.pause(backwardsID);
                }
                break;

            case AnimationTypes.Rotation:
                if (!GameManager.Instance.IsPlaying) return;
                transform.Rotate(RotationAxis * RotationSpeed * Time.deltaTime);
                break;
        }
                        
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.layer == 13) return;

        rb.useGravity = true;
        rb.WakeUp();

        switch (Type)
        {
            case AnimationTypes.MoveLooped:

                LeanTween.cancel(forwardID);
                LeanTween.cancel(backwardsID);
                AddForceToNearby();
                break;
        }

        Destroy(this);
    }

    void AddForceToNearby()
    {
        float radius = GetComponent<Renderer>().bounds.extents.magnitude;
        Collider[] colliders = Physics.OverlapSphere(transform.position, radius * 1.2f);

        foreach (Collider hit in colliders)
        {
            Rigidbody rb2 = hit.GetComponent<Rigidbody>();
            if (rb2 != null) rb2.AddForce((hit.transform.position - transform.position) * 100f * PushOnContact);
        }
    }

}
