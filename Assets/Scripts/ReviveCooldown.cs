﻿using UnityEngine;
using UnityEngine.UI;

public class ReviveCooldown : MonoBehaviour
{
    Image progressBar;

    void Start()
    {
        progressBar = GetComponent<Image>();
    }

    void Update()
    {
        progressBar.fillAmount -= 0.2f * Time.deltaTime;
    }
}
