﻿using UnityEngine;

public class LevelFinish : MonoBehaviour
{
    bool isTriggered = false;

    private void OnTriggerEnter(Collider collision)
    {
        if (collision.gameObject.layer == 11 && !isTriggered)
        {
            isTriggered = true;
            GameManager.Instance.LevelCompleted();
            
        }
    }    

}