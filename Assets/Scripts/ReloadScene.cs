﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class ReloadScene : MonoBehaviour
{
    
    public void RestartGame()
    {
        //if(!Settings.Instance.SkipAds) Advertising.main.ShowAds(AdType.Regular);
        ScoreSystem.Instance.ErasePlayerScore();
        SceneManager.LoadScene(0);
    }

    public void OnRevive() {
        //SessionManager.Instance.ContinuePlay();
    }

    public void ContinueGame()
    {
        //if(!Settings.Instance.SkipAds) Advertising.main.ShowAds(AdType.Regular);
        SceneManager.LoadScene(0);
    }
}
