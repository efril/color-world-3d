﻿using UnityEngine;
using System.Collections.Generic;
using System.Linq;

public class CameraMover : MonoBehaviour
{
    [SerializeField] private BoxCollider _checkCollider;
    List<Collider> _colliders = new List<Collider>();

    public GameObject Level
    {
        set
        {
            _colliders = value.GetComponentsInChildren<Collider>()
                .Where(n => n.GetComponent<Rigidbody>() != null)
                .Where(n => !n.GetComponent<Rigidbody>().isKinematic)
                .Where(n => n.gameObject.activeSelf)
                .OrderBy(n => n.transform.position.z)
                .ToList();
            _colliders.ForEach(p => p.gameObject.SetActive(false));

            ApplyColorSet();

            ResetColliders();
        }
    }

    public static CameraMover Instance { get; private set; }

    private void Awake()
    {
        if (Instance == null) Instance = this;      
    }

    private void Start()
    {
        if (Instance != this) Destroy(gameObject);
    }

    void Update()
    {
        if (GameManager.Instance.IsPlaying) {
            transform.Translate(MoveSpeed * Time.deltaTime, Space.World);
            ResetColliders();
        }
    }

    void ApplyColorSet()
    {
        int rndColorSet = Random.Range(0, Settings.Instance.ColorSets.Count);

        Camera.main.backgroundColor = Settings.Instance.ColorSets[rndColorSet].SpaceColor;
        GameManager.Instance.PlayerSkin.GetComponent<Renderer>().material = Settings.Instance.ColorSets[rndColorSet].PlayerBallMaterial;
        GameManager.Instance.PlayerSkin.transform.Find("SphereMesh").GetComponent<Renderer>().material = Settings.Instance.ColorSets[rndColorSet].PlayerBallMaterial;

        Renderer[] renderers = (Renderer[])Object.FindObjectsOfType(typeof(Renderer));

        for (int i = 0; i < renderers.Length; i++)
        {
            if (renderers[i].gameObject.layer == 13) renderers[i].gameObject.GetComponent<Renderer>().material = Settings.Instance.ColorSets[rndColorSet].RoadMaterial;
        }

        for (int i = 0; i < _colliders.Count; i++)
        {
            if (_colliders[i].gameObject.layer == 8) _colliders[i].gameObject.GetComponent<Renderer>().material = Settings.Instance.ColorSets[rndColorSet].BadMaterial;
            if (_colliders[i].gameObject.layer == 9) _colliders[i].gameObject.GetComponent<Renderer>().material = Settings.Instance.ColorSets[rndColorSet].NeutralMaterial;
        }

    }

    int pFirst = 0;
    int pLast = 0;
    private void ResetColliders() {
        while (pFirst < _colliders.Count && !_checkCollider.Contains(_colliders[pFirst].transform.position)) {
            _colliders[pFirst++].gameObject.SetActive(false);
            
        }

        if (pLast == 0)
            pLast = pFirst;

        while (pLast < _colliders.Count && _checkCollider.Contains(_colliders[pLast].transform.position)) {
            _colliders[pLast++].gameObject.SetActive(true);
            
        }
    }

    public Vector3 MoveSpeed {
        get => Vector3.forward * Settings.Instance.CameraSpeed * GameManager.Instance.BoosterSpeedUp();
    }
}


public static partial class Helper
{
    public static bool Contains(this BoxCollider _box, Vector3 p) {
        p = _box.transform.InverseTransformPoint(p) - _box.center;
        var size = _box.size * 0.5f;
        return /*p.x > -size.x && p.x < size.x &&
            p.y > -size.y && p.y < size.y &&*/
            p.z > -size.z && p.z < size.z;
    }
}
