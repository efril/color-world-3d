﻿using UnityEngine;

public class PlayerCollisions : MonoBehaviour
{
    Material playerMaterial;
    
    //void Start()
    //{
    //    playerMaterial = GetComponent<Renderer>().material;
    //    Debug.Log(playerMaterial);
    //}

    private void OnCollisionEnter(Collision collision)
    {
        playerMaterial = GetComponent<Renderer>().material;

        if (collision.gameObject.layer == 9) //Collided with good
        {
            if (collision.gameObject.GetComponent<MaterialPainter>() == null) collision.gameObject.AddComponent<MaterialPainter>();

            if (collision.gameObject.GetComponent<Renderer>().material == playerMaterial) return;

            collision.gameObject.GetComponent<Renderer>().material = playerMaterial;

            GameManager.Instance.SpendPaint();

            collision.gameObject.layer = 12;

            ScoreSystem.Instance.AddScore();

            return;
        }
        
    }

    private void OnCollisionStay(Collision collision)
    {
    
        if (collision.gameObject.layer == 8) //Collided with bad
        {
            GameManager.Instance.LooseLive();
            return;
        }
    }

}
