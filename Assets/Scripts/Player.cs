﻿using UnityEngine;

public class Player {
    private static Player _instance = null;
    public static Player Instance {
        get {
            if (_instance == null) {
                _instance = new Player();
            }
            return _instance;
        }
    }

    public int CurrentLevel
    {
        get { return PlayerPrefs.GetInt("Level", 0); }
        set { PlayerPrefs.SetInt("Level", value); }
    }

    public int Score {
        get { return PlayerPrefs.GetInt("Score", 0); }
        set { PlayerPrefs.SetInt("Score", value); }
    }

    public int HighScore
    {
        get { return PlayerPrefs.GetInt("HighScore", 0); }
        set { PlayerPrefs.SetInt("HighScore", value); }
    }

    public float BestZ
    {
        get { return PlayerPrefs.GetFloat("BestZ", -30f); }
        set { PlayerPrefs.SetFloat("BestZ", value); }
    }

}
