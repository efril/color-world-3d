﻿using Facebook.Unity;
using UnityEngine;
using System.Collections.Generic;

public class Settings : MonoBehaviour
{
    [Header("General")]
    public float CameraSpeed = 0.1f;
    //public Color PlayerColor = Color.blue;
    //public Material BadMaterial;
    public float BadKillsIn = 1f;
    public bool LowerSizeOnHit = false;
    [Range(0.1f, 1f)]
    public float BallMinSize = 0.33f;
    [Space]
    public float PaintLooseSpeed = 30f;
    public float PaintSpendSpeed = 10f;
    public float MaxPaint = 100f;
    public float PaintBucketHeals = 30f;

    [Header("Ball control")]
    public float DirectionChangeSpeed = 0.5f;
    public float MoveSpeed = 3f;

    [Header("Scoring")]
    public int ScoreGain = 50;
    public int BonusScore = 20;
    public float BonusTimer = 1f;

    [Header("Invulnerability")]
    public int ScoreToGain = 1000;
    public float TimeActivated = 4f;

    [Range(1f, 100f)]
    public int SpeedUpBy = 50;

    [Header("Coloring")]
    public List<ColorSet> ColorSets = new List<ColorSet>();

    [Header("Debug")]
    public bool InfiniteGodMode = false;
    public bool TestLevelMode = false;

    public static Settings Instance { get; private set; }    

    [System.Serializable]
    public class ColorSet
    {
        public Material BadMaterial;
        public Material NeutralMaterial;
        public Material PlayerBallMaterial;
        public Material RoadMaterial;
        public Color SpaceColor = Color.cyan;
    }

    private void Awake()
    {
        if (Instance == null){
            Instance = this;
            Application.targetFrameRate = 60;
            Screen.sleepTimeout = SleepTimeout.NeverSleep;
            DontDestroyOnLoad(gameObject);
        }
    }

    private void Start()
    {
        if (Instance != this) {
            Destroy(gameObject);
        } else {
            FB.Init();
        }
    }

}
